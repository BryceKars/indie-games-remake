﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BallCode : MonoBehaviour {
    public float TimeCurrent;

    public float Distance;
    public float NewDistance;

    public Transform CraneBase;

    public GameObject Text;
    public GameObject Redo;
    public GameObject Yay;

    public AudioSource groundSource;
    public AudioClip groundClip;


    public float Highest;

    // Use this for initialization

    void Start () {
		
	}

    // Update is called once per frame
    void Update()
    {
        Text.GetComponent<Text>().text = Distance.ToString("F1");

        Distance = Vector2.Distance((new Vector2(transform.position.x, transform.position.y)), (new Vector2(CraneBase.position.x, CraneBase.position.y)));

        if (Input.GetKeyDown(KeyCode.R))
        {
            Application.LoadLevel(0);
        }
       
    }



    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Floor")
        {
            groundSource.PlayOneShot(groundClip);
            // TimeCurrent = 2f;
        }

    }
    
     
    private void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Floor")
        {
            TimeCurrent -= Time.deltaTime;
            Redo.GetComponent<Text>().text = "Press 'R' to retry";

            if (TimeCurrent <= 0f)
            {
                if (PlayerPrefs.GetFloat("highscore") < Distance)
                {
                    StoreHighscore(Distance);
                }
               
            }
        }
    }

    void StoreHighscore(float newHighscore)
    {
        float oldHighscore = PlayerPrefs.GetFloat("highscore", 0);
        if (newHighscore > oldHighscore)
        {
            PlayerPrefs.SetFloat("highscore", newHighscore);
            Yay.GetComponent<Animator>().SetTrigger("IsYay");

        }
           
    }
}
