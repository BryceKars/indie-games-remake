﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HighScoreScript : MonoBehaviour {

    public BallCode BallCode;

    public GameObject HighScore;

    public float HighScoreNum;

    // Use this for initialization
    private void Awake()
    {
       
    }

    void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        HighScore.GetComponent<Text>().text = PlayerPrefs.GetFloat("highscore").ToString("F1");
    }    
}
