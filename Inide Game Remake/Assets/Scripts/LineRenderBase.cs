﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineRenderBase : MonoBehaviour {

    public Transform BaseTransform;
    public Transform SwingTransform;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        GetComponent<LineRenderer>().SetPosition(0, new Vector3(BaseTransform.transform.position.x, BaseTransform.transform.position.y, BaseTransform.transform.position.z));
        GetComponent<LineRenderer>().SetPosition(1, new Vector3(SwingTransform.transform.position.x, SwingTransform.transform.position.y, SwingTransform.transform.position.z));

    }
}
