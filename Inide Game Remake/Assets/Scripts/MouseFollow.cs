﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseFollow : MonoBehaviour {

    public Transform MousePointer;
    public Transform WeaponSprite;
    public Transform VehicalTransform;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        transform.position = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, Input.mousePosition.z + 10f));

        GetComponent<LineRenderer>().SetPosition(0, new Vector3(MousePointer.transform.position.x, MousePointer.transform.position.y, MousePointer.transform.position.z));
        GetComponent<LineRenderer>().SetPosition(1, new Vector3(VehicalTransform.transform.position.x, VehicalTransform.transform.position.y, VehicalTransform.transform.position.z));

    }
}
