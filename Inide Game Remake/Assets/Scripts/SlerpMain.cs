﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlerpMain : MonoBehaviour {

    public Transform MousePointer;
    public Transform WeaponSprite;
    public Transform VehicalTransform;
    public GameObject CraneSwing;
    public float Distance;

    public float force;

    public SmoothCamera CameraGO;

    

    private Vector3 velocity = Vector3.zero;

	// Use this for initialization
	void Start ()
    {
		

	}
	
	// Update is called once per frame
	void FixedUpdate ()
    {
        Rect screenRect = new Rect(0, 0, Screen.width + 2f, Screen.height + 2f);
        if (screenRect.Contains(Input.mousePosition) && CameraGO.CameraGO != true)
        {
            GetComponent<Rigidbody2D>().MovePosition(new Vector2(MousePointer.position.x, MousePointer.position.y));
        }

        Distance = Vector2.Distance(new Vector2(GetComponent<Transform>().transform.position.x, GetComponent<Transform>().transform.position.y), new Vector2(CraneSwing.GetComponent<Transform>().transform.position.x, CraneSwing.GetComponent<Transform>().transform.position.y));

        GetComponent<LineRenderer>().SetPosition(0, new Vector3(VehicalTransform.transform.position.x, VehicalTransform.transform.position.y, VehicalTransform.transform.position.z));
        GetComponent<LineRenderer>().SetPosition(1, new Vector3(WeaponSprite.transform.position.x, WeaponSprite.transform.position.y, WeaponSprite.transform.position.z));

    }
}
