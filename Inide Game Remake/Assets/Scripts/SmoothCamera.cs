﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmoothCamera : MonoBehaviour {

    public Transform Ball;

    public bool CameraGO;

    private Vector2 velocity = Vector2.zero;

	// Use this for initialization
	void Start ()
    {
        CameraGO = false;
	}
	
	// Update is called once per frame
	void FixedUpdate ()
    {
        Rect screenRect = new Rect(0, 0, Screen.width + 1f, Screen.height + 1f);
        if (CameraGO == true)
        {
            transform.position = Vector2.SmoothDamp(transform.position, Ball.transform.position, ref velocity, 0.01f, 500f, Time.time);
        }
        else
        {
            return;
        }
        
       

    }
}
