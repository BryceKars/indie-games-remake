﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WeaponVelocity : MonoBehaviour
{

    public Material Green;
    public Material Red;

    public SmoothCamera SmoothCamera;


    public Transform Ball;
    public float Distance;
    public float NewDistance;

    public float TimeCurrent;

    public AudioSource ballSource;
    public AudioClip ballClip;



    public float SmashForce;
    private Vector3 lastPosition;

    void Start()
    {
        TimeCurrent = 2f;
    }

    void FixedUpdate()
    {
        SmashForce = (transform.position - lastPosition).magnitude;
        lastPosition = transform.position;
        GetComponent<SpriteRenderer>().material.Lerp(Green, Red, SmashForce / 2);
        

        

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Ball")
        {
            SmoothCamera.CameraGO = true;
            ballSource.PlayOneShot(ballClip);
        }

    }
}



